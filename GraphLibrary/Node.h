#pragma once
#include <vector>
#include <string>
#include "..\Utils\rapidxml.hpp"
#include "Edge.h"

typedef std::vector<Edge*> EdgeVector ;

 class Node
{
public:
	Node(std::string& id);
	Node(rapidxml::xml_node<>  *pXmlNode);

	void AddEdge(Edge* pEdge);

	std::string& GetId() const;
	void SetId(std::string& newId);
	const EdgeVector& GetEdges() const;
	Edge* FindEdge(std::string& nodeTo);

	virtual ~Node();

private:
	std::string _id;			// node number
	EdgeVector _edges;
};

inline std::string& Node::GetId() const
{
	return const_cast<std::string&>(_id);
}
inline void Node::SetId(std::string& newId)
{
	_id = newId;
}
inline const EdgeVector& Node::GetEdges() const
{
	return _edges;
}

