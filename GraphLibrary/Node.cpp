#include "pch.h"
#include "Node.h"
#include <string>
#include <algorithm>
#include "..\Utils\rapidxml.hpp"

using namespace rapidxml;

Node::Node(std::string& id)
{
	_id = id;
}

Node::Node(xml_node<>* pXmlNode)
{
	_id = pXmlNode->first_attribute("id")->value();
}
Node::~Node()
{
	for (Edge* edge : _edges)
	{
		delete edge;
	}
	_edges.clear();
}
void Node::AddEdge(Edge* pEdge)
{
	_edges.push_back(pEdge);
}
 Edge* Node::FindEdge(std::string& nodeTo)
{
	class EdgeFinder
	{
	public:
		EdgeFinder(std::string& strTo)
		{
			compareTo = strTo;
		}
		bool operator()(Edge* Edge) const { return compareTo == Edge->GetNodeTo(); }
	private:
		std::string compareTo;
	};

	EdgeFinder finder(nodeTo);
	
	EdgeVector::iterator ifTind = std::find_if(_edges.begin(), _edges.end(), finder);
	return (*ifTind);
}

