#pragma once
#include<string>
#include "..\Utils\rapidxml.hpp"


using namespace rapidxml;

class Edge
{
public:

	Edge(int length, int time, bool blockedForTrucks);
	Edge(xml_node<>* edgeNode);
	
	void SetEdgeId(std::string& edgeId);
	std::string& GetEdgeId() ;
	void SetNodeFrom(std::string& nodeFrom);
	std::string& GetNodeFrom() ;
	void SetNodeTo(std::string& nodeTo);
	std::string& GetNodeTo() ;
	void SetLength(int length);
	int GetLength();
	void SetTravelTime(int travelTime);
	int GetTravelTime();
	bool IsBlockedForTrucks();
	void SetBlockedForTrucks(bool bIsBlocked);

	virtual ~Edge() = default;

protected:
	void initialize(xml_node<>* pEdgeNode);

private:

	std::string _edgeId;
	std::string _nodeFrom;
	std::string _nodeTo;

	int _length;
	int _travelTime;
	bool _blockedForTrucks;
};

inline void Edge::SetEdgeId(std::string& edgeId)
{
	_edgeId = edgeId;
}

inline std::string& Edge::GetEdgeId() 
{
	return _edgeId;
}

inline void Edge::SetNodeFrom(std::string& nodeFrom)
{
	_nodeFrom = nodeFrom;
}
inline std::string& Edge::GetNodeFrom()
{
	return _nodeFrom;
}
inline void Edge::SetNodeTo(std::string& nodeTo)
{
	_nodeTo = nodeTo;
}
inline std::string& Edge::GetNodeTo()
{
	return _nodeTo;
}
inline int Edge::GetLength()
{
	return _length;
}
inline void Edge::SetLength(int length)
{
	_length = length;
}
inline int Edge::GetTravelTime()
{
	return _travelTime;
}
inline void Edge::SetTravelTime(int travelTime)
{
	_travelTime = travelTime;
}
inline void Edge::SetBlockedForTrucks(bool isBlocked)
{
	_blockedForTrucks = isBlocked;
}
inline bool Edge::IsBlockedForTrucks()
{
	return _blockedForTrucks;
}