#include "pch.h"
#include "Graph.h"
#include "Route.h"
#include "..\Utils\rapidxml.hpp"
#include <string>
#include <fstream>
#include <algorithm>
#include <map>
#include "Node.h"


using namespace rapidxml;
using namespace std;

typedef map<string, Node*> NodeMap;

GRAPHLIBRARY_API Graph::Graph(std::string& fileName)
{
    buildGraph(fileName);
}

GRAPHLIBRARY_API void Graph::GetNodeById(std::string& nodeId, Node*& pNode)
{
    pNode = _nodes.at(nodeId);
}

Graph::~Graph()
{
    for (NodeMap::iterator it = _nodes.begin(); it != _nodes.end(); ++it)
    {
        delete it->second;
    }
    _nodes.clear();
}

void Graph::buildGraph(std::string& fileName)
{
    std::string xmlText;
    xml_document<> doc ;    // character type defaults to char

    appendTextFromFile(fileName, xmlText);

    doc.parse<0>(const_cast<char*>(xmlText.c_str()));    // 0 means default parse flags
    xml_node<>* root_node = doc.first_node("gxl");
    xml_node<>* graph_node = root_node->first_node("graph");

    for (xml_node<>* node = graph_node->first_node("node"); node; node = node->next_sibling("node"))
    {
        Node* pNode_ = new Node(node);
        _nodes.insert(make_pair(pNode_->GetId(), pNode_));
    }
    for (xml_node<>* edge = graph_node->first_node("edge"); edge; edge = edge->next_sibling("edge"))
    {
        std::string nodeFromId = edge->first_attribute("from")->value();
        Node* pointOfOrigin = _nodes[nodeFromId];//GetNodeById(nodeFromId);
        if (pointOfOrigin != nullptr)
        {
            Edge* pEdge_ = new Edge(edge);
            pointOfOrigin->AddEdge(pEdge_);
        }

    }
}

void Graph::appendTextFromFile(std::string& fileName, std::string& resultText)
{
    std::ifstream file(fileName.c_str());
    std::string str;
    while (std::getline(file, str)) {
        resultText += str;
    }

}

GRAPHLIBRARY_API void Graph::GetQuickestRoute(std::string& nodeFrom, std::string& nodeTo,  Route* pRoute)
{
    VisitedNodesMap visitedNodes;
    pRoute->AddNodeToRoute(_nodes.at(nodeFrom)); 
    dfs(nodeFrom, nodeTo, visitedNodes, pRoute);
    pRoute->PrintRoute();
}
void Graph::dfs(std::string& start, std::string& end, VisitedNodesMap& visitedNodes, Route* route)
{
    visitedNodes[start] = true;
    if (start == end)
    {
        route->SetRoutePossible(true);
        route->VerifyThisPath();
        //return;
    }
    Node* pNodeStart = _nodes.at(start); 
    for (Edge* pLinked_ : pNodeStart->GetEdges())
    {
        if (visitedNodes[pLinked_->GetNodeTo()] == false)
        {
            Node* pNodeNext = _nodes.at(pLinked_->GetNodeTo()); 
            route->AddNodeToRoute(pNodeNext);
            dfs(pNodeNext->GetId(), end, visitedNodes, route);
            route->RemoveNode(pNodeNext);
        }
    }
    visitedNodes[start] = false;
}