#include "pch.h"
#include "Route.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "Node.h";
#include <chrono>
#include "..\Utils\date.h"


void Route::cleanupPath(NodesPath& path)
{
	for (NodesPath::iterator it = path.begin(); it != path.end(); ++it)
	{
		delete* it;
	}
	path.clear();
}
int Route::CalculateRouteLength(NodesPath& routePath)
{
	int routeLength = 0;
	for (size_t i = 0; i < _routeNodes.size() - 1; i++)
	{
		Node* pFirst_ = _routeNodes[i];
		Node* pSecond_ = _routeNodes[i + 1];
		Edge* pConnect_ = pFirst_->FindEdge(pSecond_->GetId());
		routeLength += pConnect_->GetLength();
	}
	return routeLength;
}

int Route::CalculateRouteTime(NodesPath& routePath)
{
	int travelTime = 0;
	for (size_t i = 0; i < routePath.size() - 1; i++)
	{
		Node* pFirst_ = routePath[i];
		Node* pSecond_ = routePath[i + 1];
		Edge* pConnect_ = pFirst_->FindEdge(pSecond_->GetId());
		if (pConnect_->IsBlockedForTrucks() && _vehicleType == "truck")
		{
			travelTime = -1;
			break;
		}
		travelTime += pConnect_->GetTravelTime();
	}
	return travelTime;
}
void Route::VerifyThisPath()
{
	NodesPath copy = _routeNodes;
	_allPaths.push_back(copy);
}
void Route::PrintRoute()
{
	if (_isRoutePossible == false)
	{
		std::cout << "No route possible!";
	}
	else
	{
		int pathIndex=-1, minPathTravelTime=0;
		for (int i = 0; i < _allPaths.size(); ++i)
		{
			int travelTime = CalculateRouteTime(_allPaths.at(i));
			if (travelTime == -1)
			{
				// we found a route blocked for trucks!
				continue;
			}
			if ((minPathTravelTime == 0) || (travelTime < minPathTravelTime))
			{
				minPathTravelTime = travelTime;
				pathIndex = i;
			}
		}

		if (pathIndex == -1)
		{
			// no possible routes!
			std::cout << "No route possible!";
			return;
		}

		// print out nodes and edges of the route:
		
		NodesPath quickest_ = _allPaths.at(pathIndex);
		_routeNodes = quickest_;
		_routeTime = minPathTravelTime;

		std::cout << "Route:";
		for (size_t i = 0; i < _routeNodes.size(); i++)
		{
			std::cout << _routeNodes[i]->GetId();
			if (i != _routeNodes.size() - 1)
			{
				Node* pNext_ = _routeNodes[i + 1];
				Edge* pConnect_ = _routeNodes[i]->FindEdge(pNext_->GetId());
				std::cout << " -> " << pConnect_->GetEdgeId() << " -> ";
			}
		}
		std::cout << std::endl;

		// print out the distance of the route:
		_routeLength = CalculateRouteLength(quickest_);
		std::cout << "Distance: " << _routeLength << "m" << std::endl;

		// printout ETA:
		std::string eta;
		calculateAndFormatEta(eta);
		std::cout << "ETA: " << eta << std::endl;

		
	}
}
void Route::calculateAndFormatEta(std::string& formatedEta)
{
	using namespace std;
	vector<string> splitStartTime;
	istringstream f(_startTime);
	string s;
	while (getline(f, s, ':')) {
		splitStartTime.push_back(s);
	}
	int hours, minutes, seconds = 0;
	hours = atoi(splitStartTime.at(0).c_str());
	minutes = atoi(splitStartTime.at(1).c_str());
	if (splitStartTime.size() == 3)
	{
		seconds = atoi(splitStartTime.at(2).c_str());
	}
	
	int totalTime = hours * 3600 + minutes * 60 + seconds + _routeTime;
	int etaHours = totalTime / 3600;
	int etaMinutes = (totalTime % 3600) / 60;
	int etaSeconds = (totalTime % 3600) % 60;
	if (etaHours > 24) etaHours = etaHours - 24;
	if (etaSeconds > 0) etaMinutes += 1;
	std::string formatedHours = etaHours < 10 ? "0" + to_string(etaHours) : to_string(etaHours);
	std::string formatedMinutes = etaMinutes < 10 ? "0" + to_string(etaMinutes) : to_string(etaHours);

	formatedEta = formatedHours + ":" + formatedMinutes;
	
}

