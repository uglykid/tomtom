#pragma once
#include <vector>
#include "Node.h"
#include "Route.h"
#include <string>
#include <map>

typedef std::map<std::string, bool> VisitedNodesMap;

class GRAPHLIBRARY_API Graph
{
public:
	Graph(std::string& fileName);
	virtual ~Graph();

	void GetNodeById(std::string& nodeId, Node*& pNode);
	void GetQuickestRoute(std::string& nodeFrom, std::string& nodeTo, Route* pRoute);

protected:
	void buildGraph(std::string& fileName);
	void appendTextFromFile(std::string& fileName, std::string& resultText);
	void dfs(std::string& start, std::string& end, VisitedNodesMap& visitedNodes, Route* route);


	
private:
	std::map<std::string, Node*> _nodes;
};

