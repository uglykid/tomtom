#pragma once
#include "pch.h"
#include <vector>
#include <string>
#include <deque>

class Node;

typedef std::deque<Node*> NodesPath;

class GRAPHLIBRARY_API  Route
{
public:
	virtual ~Route() = default;

	void AddNodeToRoute(Node* pNode);
	void RemoveNode(Node* pNode);

	void SetRouteLength(int length);
	void SetRouteTime(int time);
	void SetRoutePossible(bool isPossible);
	void SetRouteStartTime(std::string& startTime);
	void SetVehicleType(std::string& type);


	int GetRouteLength();
	int GetRouteTime();
	bool IsRoutePossible();
	std::string& GetRouteStartTime();
	std::string& GetVehicleType();
	
	int CalculateRouteLength(NodesPath& nPath);
	int CalculateRouteTime(NodesPath& nPath);
	void VerifyThisPath();
	void PrintRoute();

protected:
	void calculateAndFormatEta(std::string& formatedEta);
	void cleanupPath(NodesPath& path);

private:

	NodesPath _routeNodes;
	std::vector<NodesPath> _allPaths;
	
	std::string _startTime;
	bool _isRoutePossible;
	int _routeLength;
	int _routeTime;
	std::string _vehicleType;
};

inline void Route::AddNodeToRoute(Node* pNode)
{
	_routeNodes.push_back(pNode);
}
inline void Route::RemoveNode(Node* pNode)
{
	_routeNodes.pop_back();
}
inline void Route::SetRouteLength(int length)
{
	_routeLength = length;
}
inline int Route::GetRouteLength()
{
	return _routeLength;
}
inline void Route::SetRouteTime(int time)
{
	_routeTime = time;
}
inline int Route::GetRouteTime()
{
	return _routeTime;
}
inline void Route::SetRoutePossible(bool isPossible)
{
	 _isRoutePossible = isPossible;
}
inline bool Route::IsRoutePossible()
{
	return _isRoutePossible;
}
inline void Route::SetRouteStartTime(std::string& time)
{
	_startTime = time;
}
inline std::string& Route::GetRouteStartTime()
{
	return _startTime;
}
inline void Route::SetVehicleType(std::string& type)
{
	_vehicleType = type;
}
inline std::string& Route::GetVehicleType()
{
	return _vehicleType;
}



