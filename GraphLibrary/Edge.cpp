#include "pch.h"
#include "Edge.h"
#include "..\Utils\rapidxml.hpp"


using namespace rapidxml;

Edge::Edge(int length, int time, bool blockedForTrucks)
{
	_length = length;
	_travelTime = time;
	_blockedForTrucks = blockedForTrucks;
}

Edge::Edge(xml_node<>* pEdgeNode)
{
	initialize(pEdgeNode);
}

void Edge::initialize(xml_node<>* pEdgeNode)
{
	// attributes of the 'root' edge node:

	_edgeId = pEdgeNode->first_attribute("id")->value();
	_nodeFrom = pEdgeNode->first_attribute("from")->value();
	_nodeTo = pEdgeNode->first_attribute("to")->value();

	// iterate thru children to fetch the edge's parameters : 

	_blockedForTrucks = false;
	
	for (xml_node<>* pEdgeChild = pEdgeNode->first_node("attr"); pEdgeChild != nullptr; pEdgeChild = pEdgeChild->next_sibling("attr"))
	{
		xml_node<>* pParamValue_ = nullptr;
		
		std::string type_ = pEdgeChild->first_attribute("name")->value();
		if (type_=="length") 
		{
			pParamValue_ = pEdgeChild->first_node("int");
			_length = atoi(pParamValue_->value());
		}
		else if (type_ == "traveltime")
		{
			pParamValue_ = pEdgeChild->first_node("int");
			_travelTime = atoi(pParamValue_->value());
		}
		else if(type_ == "isblockedfortrucks")
		{
			pParamValue_ = pEdgeChild->first_node("bool");
			std::string value = pParamValue_->value();
			_blockedForTrucks = value == "true";
		}
	}

}
