// TomTomGraphs.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <fstream>
#include "..\GraphLibrary\Route.h"
#include "..\GraphLibrary\framework.h"
#include "..\GraphLibrary\Graph.h"


void parseTestFile(std::string& file);

int main(int argc, char* argv[])
{
    // expected usage: TomTomGraphs <input_graph_file> <input_routes_file>
    if (argc != 3)
    {
        std::cout << "You must provide location of the graph description and the routes input file.";
        return -1;
    }
    else
    {
        std::string fileName = argv[1]; 
        std::string routesFile = argv[2]; 

        Graph roadmapGraph(fileName);
        Route* pThisRoute_ = nullptr;

        int linesRead = 0;
        std::ifstream routes(routesFile.c_str());
        std::string line;
        
        std::string fromNode, nodeTo, startTime;
        

        while (std::getline(routes, line))
        {
            int whatLine_ = linesRead % 4;
            switch (whatLine_)
            {
                case 0:
                    pThisRoute_ = new Route();
                    pThisRoute_->SetRouteStartTime(line.append(":00"));
                    break;
                case 1:
                    fromNode = line;
                    break;
                case 2:
                    nodeTo = line;
                    break;
                case 3:
                    pThisRoute_->SetVehicleType(line);
                    roadmapGraph.GetQuickestRoute(fromNode, nodeTo,  pThisRoute_);
                    delete pThisRoute_;
                    break;
                default:
                    break;
            }
            linesRead++;
        }


    }
}



// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
